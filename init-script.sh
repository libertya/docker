#!/bin/bash

#set -Eeo pipefail
# TODO add "-u"

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}



ini_env() {
		#Libertya home
		file_env 'OXP_HOME' "${OXP_HOME:-/opt/ServidorOXP}"

		#Java home
  		file_env 'JAVA_HOME' "${JAVA_HOME}"

		#Type of jvm, sun|mac|<ibm>
		file_env 'TIPO_JAVA_OXP' 'sun'
		#Java runtime options
		file_env 'OPCIONES_JAVA_OXP' '-Xms1024M -Xmx1536M -XX:MaxPermSize=1024M -Dfile.encoding=UTF-8'

		#Type of database, postgresql|oracle|oracleXE|fyracle
		file_env 'TIPO_BD_OXP' 'PostgreSQL'
		#Database server host name
		file_env 'SERVIDOR_BD_OXP' "${SERVIDOR_BD_OXP:-postgres}"
		#Database port, oracle[1512], postgresql[5432], fyracle[3500]
		file_env 'PUERTO_BD_OXP' "${PUERTO_BD_OXP:-5432}"
		#Database name
		file_env 'NOMBRE_BD_OXP' "${NOMBRE_BD_OXP:-libertya}"
		#Database system user password
		file_env 'SYSTEM_BD_OXP' "${NOMBRE_BD_OXP:-postgres}"
		#Database user name
		file_env 'USUARIO_BD_OXP' "${USUARIO_BD_OXP:-libertya}"
		#Database user password
		file_env 'PASSWD_BD_OXP' "${PASSWD_BD_OXP:-libertya}"
		#Database URL
		file_env 'URL_BD_OXP' 'jdbc\:postgresql\://'${SERVIDOR_BD_OXP:-postgres}'\:'${PUERTO_BD_OXP:-5432}'/'${NOMBRE_BD_OXP:-libertya}

		#Type of application server
		file_env 'TIPO_APPS_OXP' "${TIPO_APPS_OXP:-jboss}"
		#Application server host name
		file_env 'SERVIDOR_APPS_OXP' "${SERVIDOR_APPS_OXP:-localhost}"
		#Application server port
		file_env 'PUERTO_JNP_OXP' "${PUERTO_JNP_OXP:-1099}"
		file_env 'PUERTO_WEB_OXP' "${PUERTO_WEB_OXP:-8080}"
		file_env 'PUERTO_SSL_OXP' "${PUERTO_SSL_OXP:-8443}"
		#Deploy location
		file_env 'DEPLOY_APPS_OXP' "${DEPLOY_APPS_OXP:-/opt/ServidorOXP/jboss/server/openXpertya/deploy}"

		#12 setting
		file_env 'KEYSTORE_OXP' "${KEYSTORE_OXP:-/opt/ServidorOXP/keystore/myKeystore}"
		file_env 'ALIASWEBKEYSTORE_OXP' "${ALIASWEBKEYSTORE_OXP:-libertya}"
		file_env 'CODIGOALIASKEYSTORE_OXP' "${CODIGOALIASKEYSTORE_OXP:-libertya}"
		file_env 'KEYSTOREPASS_OXP' "${KEYSTOREPASS_OXP:-myPassword}"

		#Certificate details
		#Common name, default to host name
		file_env 'OXP_CERT_CN' "${OXP_CERT_CN:-localhost}"
		#Organization, default to the user name
		file_env 'OXP_CERT_ORG' "${OXP_CERT_ORG:-Libertya.org}"
		#Organization Unit, default to UsuarioLibertya
		file_env 'OXP_CERT_ORG_UNIT' "${OXP_CERT_ORG_UNIT:-UsuarioLibertya}"
		#Town
		file_env 'OXP_CERT_LOCATION' "${OXP_CERT_LOCATION:-MiCiudad}"
		#State
		file_env 'OXP_CERT_STATE' "${OXP_CERT_STATE:-ER}"
		#2 character country code
		file_env 'OXP_CERT_COUNTRY' "${OXP_CERT_COUNTRY:-AR}"

		#Mail server setting
		file_env 'SERVIDOR_MAIL_OXP' "${SERVIDOR_MAIL_OXP:-localhost}"
		file_env 'ADMIN_MAIL_OXP' "${SERVIDOR_MAIL_OXP:-admin@host.com}"
		file_env 'USUARIO_MAIL_OXP' "${USUARIO_MAIL_OXP:- }"
		file_env 'PASSWORD_MAIL_OXP' "${PASSWORD_MAIL_OXP:- }"

		#ftp server setting
		file_env 'SERVIDOR_FTP_OXP' "${SERVIDOR_FTP_OXP:-localhost}"
		file_env 'PREFIJO_FTP_OXP' "${PREFIJO_FTP_OXP:-my}"
		file_env 'USUARIO_FTP_OXP' "${USUARIO_FTP_OXP:-anonymous}"
		file_env 'PASSWD_FTP_OXP' "${PASSWD_FTP_OXP:-user@host.com}"


		#echo "$RAILS_ENV:" > LibertyaEnv.properties
		touch LibertyaEnv.properties
		for var in \
			OXP_HOME \
			JAVA_HOME \
			TIPO_JAVA_OXP \
			OPCIONES_JAVA_OXP \
			TIPO_BD_OXP \
			SERVIDOR_BD_OXP \
			PUERTO_BD_OXP \
			NOMBRE_BD_OXP \
			SYSTEM_BD_OXP \
			USUARIO_BD_OXP \
			PASSWD_BD_OXP \
			URL_BD_OXP \
			TIPO_APPS_OXP \
			SERVIDOR_APPS_OXP \
			PUERTO_JNP_OXP \
			PUERTO_WEB_OXP \
			PUERTO_SSL_OXP \
			DEPLOY_APPS_OXP \
			KEYSTORE_OXP \
			ALIASWEBKEYSTORE_OXP \
			CODIGOALIASKEYSTORE_OXP \
			KEYSTOREPASS_OXP \
			OXP_CERT_CN \
			OXP_CERT_ORG \
			OXP_CERT_ORG_UNIT \
			OXP_CERT_LOCATION \
			OXP_CERT_STATE \
			OXP_CERT_COUNTRY \
 			SERVIDOR_MAIL_OXP \
			ADMIN_MAIL_OXP \
			USUARIO_MAIL_OXP \
 			PASSWORD_MAIL_OXP \
			SERVIDOR_FTP_OXP \
			PREFIJO_FTP_OXP \
			USUARIO_FTP_OXP \
			PASSWD_FTP_OXP \
		; do
			env="${var^^}"
			val="${!env}"
			[ -n "$val" ] || continue
			echo "$var=$val" >> LibertyaEnv.properties
		done
}

#Cambiamos al directorio 
cd $OXP_HOME

#Inicializamos la configuracion
if [ -f 'LibertyaEnv.properties' ]; then
	rm LibertyaEnv.properties
fi

ini_env

#Confioguraciones
./ConfigurarAuto.sh

#Inicializamos Servidor
./utils/IniciarServidor.sh
