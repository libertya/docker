FROM debian:stable

RUN apt-get update && apt-get install -yq --no-install-recommends unzip

RUN useradd -ms /bin/bash libertya

COPY ServidorOXP_V17.05.zip /tmp
COPY default-java.tar.gz /tmp

RUN unzip /tmp/ServidorOXP_V17.05.zip -d /opt/

RUN mkdir -p /usr/lib/jvm
RUN tar -xvf /tmp/default-java.tar.gz -C /usr/lib/jvm

COPY init-script.sh /opt/ServidorOXP/init-script.sh

RUN chown -R libertya:libertya /opt/ServidorOXP
RUN chmod -R +x /opt/ServidorOXP/*.sh
RUN mkdir /var/run/libertya
RUN chown libertya:libertya /var/run/libertya

USER libertya
ENV OXP_HOME=/opt/ServidorOXP
ENV JAVA_HOME=/usr/lib/jvm/jdk1.8.0_321
ENV PATH="${PATH}:${JAVA_HOME}/bin"

RUN mkdir -p /opt/ServidorOXP/keystore

CMD bash /opt/ServidorOXP/init-script.sh

EXPOSE 1099
EXPOSE 8080
EXPOSE 8443